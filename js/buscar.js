function requestDatos(elementId) {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums";
    const res = document.getElementById('lista');

    if (elementId.length === 0) {
        res.innerHTML = "";
    } else {
        http.onreadystatechange = function () {
            if (this.status == 200 && this.readyState == 4) {
                var datos = JSON.parse(this.responseText);
                buscarPorId(elementId, datos, res);
            } 
        };
        http.open('GET', url, true);
        http.send();
    }
}

function buscarPorId(idEle, obJSON, res) {
    res.innerHTML = "";  // Limpiar contenido previo

    for (var i of obJSON) {
        if (parseInt(i.id) === parseInt(idEle)) {
            res.innerHTML += '<tr> <td class="columna1">' + i.userId + '</td>'
                + ' <td class="columna2">' + i.id + '</td>'
                + ' <td class="columna3">' + i.title + '</td></tr>';
        }   
    }
}